package com.example.springzoolpoc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(
            ServerHttpSecurity http,
            ReactiveAuthenticationManager jwtAuthenticatinManager,
            ServerAuthenticationConverter jwtServerAuthenticationConverter,
            JwtAuthenticationExceptionHandler jwtAuthenticationExceptionHandler) {

        var authenticationWebFilter = new AuthenticationWebFilter(jwtAuthenticatinManager);
        authenticationWebFilter.setServerAuthenticationConverter(jwtServerAuthenticationConverter);

        http
                .authorizeExchange()
                .pathMatchers("/auth/token").permitAll()
                .pathMatchers("/corp/**").permitAll()
                .pathMatchers("/customer/**").authenticated()
                .and()
                .addFilterAt(authenticationWebFilter, SecurityWebFiltersOrder.AUTHENTICATION)
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationExceptionHandler)
                .and()
                .csrf().disable()
                .httpBasic().disable()
                .logout().disable();

        return http.build();
    }
}
