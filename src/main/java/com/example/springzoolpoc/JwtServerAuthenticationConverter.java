package com.example.springzoolpoc;

import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.Map;

@Component
public class JwtServerAuthenticationConverter implements ServerAuthenticationConverter {

    private final OAuth2ResourceServerProperties serverProperties;

    public JwtServerAuthenticationConverter(OAuth2ResourceServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    @Override
    public Mono<Authentication> convert(ServerWebExchange serverWebExchange) {
        var authorizationHeaderList =
              serverWebExchange.getRequest().getHeaders().get("Authorization");

        if (authorizationHeaderList == null || authorizationHeaderList.isEmpty())
            return Mono.empty();

        try {
            return Mono.just(decodeBearerToken(authorizationHeaderList.get(0)));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return Mono.empty();
        }
    }

    private JwtAuthenticationToken decodeBearerToken(String bearerToken) {
        var issuerUri = serverProperties.getJwt().getIssuerUri();

        NimbusJwtDecoder jwtDecoder = JwtDecoders.fromOidcIssuerLocation(issuerUri);
        jwtDecoder.setClaimSetConverter(new UsernameSubClaimAdapter());

        var token = bearerToken.replace("Bearer ", "");
        var jwtAuthentication = new JwtAuthenticationToken(jwtDecoder.decode(token));
        var name = jwtAuthentication.getName();
        jwtAuthentication.setAuthenticated(true);
        return jwtAuthentication;
    }

    static class UsernameSubClaimAdapter implements Converter<Map<String, Object>, Map<String, Object>> {

        private final MappedJwtClaimSetConverter delegate = MappedJwtClaimSetConverter.withDefaults(Collections.emptyMap());

        @Override
        public Map<String, Object> convert(Map<String, Object> claims) {
            Map<String, Object> convertedClaims = this.delegate.convert(claims);
            String username = (String) convertedClaims.get("preferred_username");
            convertedClaims.put("sub", username);
            return convertedClaims;
        }
    }
}
