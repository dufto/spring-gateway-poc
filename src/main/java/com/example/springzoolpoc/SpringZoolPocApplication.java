package com.example.springzoolpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(UriConfiguration.class)
public class SpringZoolPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringZoolPocApplication.class, args);
    }

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder, UriConfiguration uriConfiguration) {
        return builder.routes()
                .route(p -> p
                        .path("/customer")
                        .filters(f -> f.rewritePath("/customer", "/"))
                        .uri(uriConfiguration.getCustomerApiEndpoint()))
                .route(p -> p
                        .path("/customer/search")
                        .filters(f -> f
                                .rewritePath("/customer/", "/"))
                        .uri(uriConfiguration.getCustomerApiEndpoint()))
                .route(p -> p
                        .path("/corp/cities/**")
                        .uri(uriConfiguration.getCorpApiEndpoint()))
                .route(p -> p
                        .path("/auth/token")
                        .filters(f -> f.setPath("/auth/realms/master/protocol/openid-connect/token"))
                        .uri(uriConfiguration.getKeycloakEndpoint()))
                .build();
    }
}

@ConfigurationProperties
class UriConfiguration {

    private String customerApiEndpoint = "http://localhost:8081";
    private String corpApiEndpoint = "http://192.168.96.21:9013";
    private String keycloakEndpoint = "http://localhost:8080";

    public String getCorpApiEndpoint() {
        return corpApiEndpoint;
    }

    public void setCorpApiEndpoint(String corpApiEndpoint) {
        this.corpApiEndpoint = corpApiEndpoint;
    }

    public String getCustomerApiEndpoint() {
        return customerApiEndpoint;
    }

    public void setCustomerApiEndpoint(String customerApiEndpoint) {
        this.customerApiEndpoint = customerApiEndpoint;
    }

    public String getKeycloakEndpoint() {
        return keycloakEndpoint;
    }

    public void setKeycloakEndpoint(String keycloakEndpoint) {
        this.keycloakEndpoint = keycloakEndpoint;
    }
}